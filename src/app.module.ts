import { Module } from '@nestjs/common'
import { ScheduleModule } from '@nestjs/schedule'
import { ServeStaticModule } from '@nestjs/serve-static'

import { join } from 'path'

import { AppController } from './app.controller'
import { CommonModule } from './common/common.module'
import { ConfigModule } from './config/config.module'
import { CredentialOfferModule } from './credential-offer/credential-offer.module'
import { JwtModule } from './jwt/jwt.module'

@Module({
  imports: [
    CredentialOfferModule,
    CommonModule,
    ConfigModule,
    JwtModule,
    ScheduleModule.forRoot(),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'src/static'),
      serveRoot: process.env['APP_PATH'] ? process.env['APP_PATH'] : '/',
      exclude: ['/api*']
    })
  ],
  controllers: [AppController]
})
export class AppModule {}
