import { HttpModule } from '@nestjs/axios'
import { ConflictException } from '@nestjs/common'
import { Test } from '@nestjs/testing'

import { DidResolver } from '@gaia-x/json-web-signature-2020'

import { CommonModule } from '../common.module'
import { DidResolverProvider } from '../providers/did-resolver.provider'
import { DIDService } from './did.service'

describe('DIDService', () => {
  let didService: DIDService
  const didResolverMock = new DidResolver()
  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [CommonModule, HttpModule],
      providers: [DidResolverProvider.create(), DIDService]
    })
      .overrideProvider(DidResolver)
      .useValue(didResolverMock)
      .compile()
    didService = moduleFixture.get(DIDService)
  })
  it('should throw an exception when the did is not resolvable', async () => {
    // Given the did is not resolvable
    jest.spyOn(didResolverMock, 'resolve').mockImplementation(() => {
      throw new Error('Unable to resolve domain')
    })
    // When getting the DID from the service
    // Then I get a Conflict Exception
    try {
      await didService.getDIDDocumentFromDID('did:web:toto.eu')
      fail()
    } catch (error) {
      expect(error).toBeDefined()
      expect(error).toBeInstanceOf(ConflictException)
      expect(error.message).toMatch(/Unable to retrieve your did .+ Unable to resolve domain/)
    }
  })
  it('should throw an exception when the did verificationMethod is absent', async () => {
    //Given the DID does not contain a field verificationMethod
    jest.spyOn(didResolverMock, 'resolve').mockImplementation((did: string) => {
      return new Promise((resolve, reject) => {
        resolve({
          '@context': ['https://www.w3.org/ns/did/v1'],
          id: 'did:web:example.com'
        })
      })
    })
    //When I'm getting the DID
    //Then I get a Conflict Exception
    try {
      await didService.getDIDDocumentFromDID('did:web:toto.eu')
      fail()
    } catch (error) {
      expect(error).toBeDefined()
      expect(error).toBeInstanceOf(ConflictException)
      expect(error.message).toMatch(/DID .* does not contain the verificationMethod array/)
    }
  })
  it('should retrieve DID using did-resolver on first call', async () => {
    //Given the DID does not contain a field verificationMethod
    jest.spyOn(didResolverMock, 'resolve').mockImplementation((did: string) => {
      return new Promise((resolve, reject) => {
        resolve({
          '@context': ['https://www.w3.org/ns/did/v1'],
          id: 'did:web:example.com',
          verificationMethod: [
            {
              id: 'did:web:example.com#key',
              type: 'JsonWebKey2020',
              controller: 'did:web:example.com'
            }
          ]
        })
      })
    })
    //When I'm getting the DID
    //Then I get a Conflict Exception
    try {
      await didService.getDIDDocumentFromDID('did:web:toto.eu')
      expect(didResolverMock.resolve).toHaveBeenNthCalledWith(1, 'did:web:toto.eu')
    } catch (error) {
      fail()
    }
  })
  it('should retrieve DID using cache on after first call', async () => {
    //Given the DID does not contain a field verificationMethod
    jest.spyOn(didResolverMock, 'resolve').mockImplementation((did: string) => {
      return new Promise((resolve, reject) => {
        resolve({
          '@context': ['https://www.w3.org/ns/did/v1'],
          id: 'did:web:example.com',
          verificationMethod: [
            {
              id: 'did:web:example.com#key',
              type: 'JsonWebKey2020',
              controller: 'did:web:example.com'
            }
          ]
        })
      })
    })
    //When I'm getting the DID
    //Then I get a Conflict Exception
    try {
      await didService.getDIDDocumentFromDID('did:web:toto.eu')
      await didService.getDIDDocumentFromDID('did:web:toto.eu')
      expect(didResolverMock.resolve).toHaveBeenNthCalledWith(1, 'did:web:toto.eu')
    } catch (error) {
      fail()
    }
  })
})
