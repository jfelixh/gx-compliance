import { ServiceUnavailableException } from '@nestjs/common'

import { DateTime } from 'luxon'
import { pki } from 'node-forge'

import { CertificateBuilderUtilsSpec } from '../../tests/certificate-builder-utils.spec'
import { ExpirationDateService } from './expiration-date.service'
import { TimeService } from './time.service'

describe('ExpirationDateService', () => {
  let expirationDateService
  let timeService

  beforeEach(() => {
    timeService = new TimeService()
    expirationDateService = new ExpirationDateService(timeService)
  })
  describe('isCertificateExpired', () => {
    it('should return true if the certificate end date is before today', async () => {
      const currentTime = new Date()
      jest.spyOn(timeService, 'getNtpTime').mockImplementation(() => currentTime)
      const cert = CertificateBuilderUtilsSpec.createCertificate(-1)
      const isExpired = await expirationDateService.isCertificateExpired(currentTime, cert.validity.notAfter)
      expect(isExpired).toBeTruthy()
    })
    it('should return false if the certificate end date is after today', async () => {
      const currentTime = new Date()
      jest.spyOn(timeService, 'getNtpTime').mockImplementation(() => currentTime)
      const cert = CertificateBuilderUtilsSpec.createCertificate(1)
      const isExpired = await expirationDateService.isCertificateExpired(currentTime, cert.validity.notAfter)
      expect(isExpired).toBeFalsy()
    })
  })

  describe('isCertificateExpiringSoon', () => {
    it('should return true if the certificate end date is before today plus threshold', async () => {
      const currentTime = new Date()
      jest.spyOn(timeService, 'getNtpTime').mockImplementation(() => currentTime)
      const cert = CertificateBuilderUtilsSpec.createCertificate(2)
      const isCertificateExpiringSoon = await expirationDateService.isCertificateExpiringSoon(currentTime, cert.validity.notAfter, 90)
      expect(isCertificateExpiringSoon).toBeTruthy()
    })
    it('should return false if the certificate end date is after today plus threshold', async () => {
      const currentTime = new Date()
      jest.spyOn(timeService, 'getNtpTime').mockImplementation(() => currentTime)
      const cert = CertificateBuilderUtilsSpec.createCertificate(4)
      const isCertificateExpiringSoon = await expirationDateService.isCertificateExpiringSoon(currentTime, cert.validity.notAfter, 90)
      expect(isCertificateExpiringSoon).toBeFalsy()
    })
  })

  describe('daysBeforeCertExpiration', () => {
    it('should return a negative number if the certificate end date is before today plus threshold', async () => {
      const currentTime = new Date()
      jest.spyOn(timeService, 'getNtpTime').mockImplementation(() => currentTime)
      const cert = CertificateBuilderUtilsSpec.createCertificate(-1)
      const daysBeforeCertExpiration = await expirationDateService.daysBeforeCertExpiration(currentTime, cert.validity.notAfter)
      expect(daysBeforeCertExpiration <= 0).toBeTruthy()
    })
    it('should return a positive number if the certificate end date is after today plus threshold', async () => {
      const currentTime = new Date()
      jest.spyOn(timeService, 'getNtpTime').mockImplementation(() => currentTime)
      const cert = CertificateBuilderUtilsSpec.createCertificate(4)
      const daysBeforeCertExpiration = await expirationDateService.daysBeforeCertExpiration(currentTime, cert.validity.notAfter)
      expect(daysBeforeCertExpiration > 0).toBeTruthy()
    })
  })

  describe('getExpirationDateBasedOnCertOrThreshold', () => {
    it('should return the end date of the certificate when the end date is after it', async () => {
      const currentTime = new Date()
      jest.spyOn(timeService, 'getNtpTime').mockImplementation(() => currentTime)
      const cert = CertificateBuilderUtilsSpec.createCertificate(1)
      const expirationDate = await expirationDateService.getExpirationDateBasedOnCertOrThreshold(pki.certificateToPem(cert), 90)
      expect(DateTime.fromJSDate(expirationDate).startOf('day').equals(DateTime.fromJSDate(cert.validity.notAfter).startOf('day'))).toBeTruthy()
    })
    it('should return the end date from threshold when certificate expiration date is after threshold', async () => {
      const currentTime = new Date()
      jest.spyOn(timeService, 'getNtpTime').mockImplementation(() => currentTime)
      const cert = CertificateBuilderUtilsSpec.createCertificate(6)
      const expirationDate = await expirationDateService.getExpirationDateBasedOnCertOrThreshold(pki.certificateToPem(cert), 90)
      const nowPlusThreshold = DateTime.fromJSDate(currentTime).plus({ days: 90 })

      expect(DateTime.fromJSDate(expirationDate).startOf('day').equals(nowPlusThreshold.startOf('day'))).toBeTruthy()
    })
    it('should throw an exception when the certificate expiration date is in the past', async () => {
      const currentTime = new Date()
      jest.spyOn(timeService, 'getNtpTime').mockImplementation(() => currentTime)
      const cert = CertificateBuilderUtilsSpec.createCertificate(-1)
      try {
        await expirationDateService.getExpirationDateBasedOnCertOrThreshold(pki.certificateToPem(cert), 90)
        fail()
      } catch (error) {
        expect(error).toBeDefined()
        expect(error).toBeInstanceOf(ServiceUnavailableException)
      }
    })
  })
})
