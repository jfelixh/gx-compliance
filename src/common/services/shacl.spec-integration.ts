import { HttpModule } from '@nestjs/axios'
import { ConfigService } from '@nestjs/config'
import { Test, TestingModule } from '@nestjs/testing'

import { readFileSync } from 'fs'
import { HttpHandler, HttpResponse, http } from 'msw'
import { SetupServerApi, setupServer } from 'msw/node'
import * as path from 'node:path'
import { DatasetCore } from 'rdf-js'

import { ConfigServiceMock } from '../../tests/config.service.mock'
// Fixtures
import {
  FailingParticipantVP,
  ParticipantVP,
  ServiceOfferingVP,
  ServiceOfferingVPProvidedByAbsent,
  ServiceOfferingVPStructureInvalid
} from '../../tests/fixtures'
import { CommonModule } from '../common.module'
import { ShaclService } from './shacl.service'

export const expectedErrorResult = expect.objectContaining({
  conforms: false,
  results: expect.arrayContaining([expect.any(String)])
})

export const expectedValidResult = expect.objectContaining({
  conforms: true
})

const CONTEXT_URL = 'https://test.gaia-x.eu/registry/shapes/development'
describe('ShaclService', () => {
  let shaclService: ShaclService
  let registryMock: SetupServerApi

  const expectedDatasetObject: DatasetCore = {
    size: expect.any(Number),
    has: expect.any(Function),
    delete: expect.any(Function),
    add: expect.any(Function),
    match: expect.any(Function),
    [Symbol.iterator]: expect.any(Object)
  }

  const participantSDRaw = JSON.stringify(ParticipantVP)
  const failingParticipantSDRaw = JSON.stringify(FailingParticipantVP)

  beforeAll(async () => {
    const handlers: HttpHandler[] = [
      http.get(CONTEXT_URL, () => {
        const standardConformityShapes: string = readFileSync(path.join(__dirname, '../../tests/fixtures/shapes/service-offering.ttl'), 'utf-8')
        return HttpResponse.text(standardConformityShapes)
      })
    ]
    registryMock = setupServer(...handlers)
    registryMock.listen()

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [CommonModule, HttpModule]
    })
      .overrideProvider('privateKey')
      .useValue(null)
      .overrideProvider(ConfigService)
      .useValue(new ConfigServiceMock({ WEB_DOCUMENT_LOADER: 'true', REGISTRY_URL: 'https://test.gaia-x.eu/registry' }))
      .compile()
    shaclService = moduleFixture.get<ShaclService>(ShaclService)
  })

  afterAll(() => {
    registryMock.close()
  })

  describe('SHACL dataset transformation of raw data', () => {
    it('transforms a dataset correctly from JsonLD input', async () => {
      const dataset = await shaclService.loadFromJSONLDWithQuads(JSON.parse(participantSDRaw))
      expectDatasetKeysToExist(dataset)
    })
  })
  describe('SHACL Shape Validation of a Self Descriptions', () => {
    it('returns true for a VerifiablePresentation using the correct shape', async () => {
      const sdDataset = await shaclService.loadFromJSONLDWithQuads(shaclService.extractCredentialSubjectsFromVerifiablePresentation(ParticipantVP))

      const validationResult = await shaclService.validate(await shaclService.loadShaclFromUrl(CONTEXT_URL), sdDataset)

      expect(validationResult).toEqual(expectedValidResult)
    })
    it('returns false for a VerifiablePresentation using the incorrect format', async () => {
      const sdDataset = await shaclService.loadFromJSONLDWithQuads(
        shaclService.extractCredentialSubjectsFromVerifiablePresentation(FailingParticipantVP)
      )

      const validationResult = await shaclService.validate(await shaclService.loadShaclFromUrl(CONTEXT_URL), sdDataset)

      expect(validationResult).toEqual(expectedErrorResult)
    })
  })

  describe('SHACL Shape Validation of a ServiceOffering', () => {
    it('returns true for a Serviceoffering using the correct shape', async () => {
      const serviceOffering = await shaclService.loadFromJSONLDWithQuads(ServiceOfferingVP)
      const shapes = await shaclService.loadShaclFromUrl(CONTEXT_URL)
      const validationResult = await shaclService.validate(shapes, serviceOffering)

      expect(validationResult).toEqual(expectedValidResult)
    })
    it('returns false ServiceOffering without proper providedBy', async () => {
      const serviceOffering = await shaclService.loadFromJSONLDWithQuads(ServiceOfferingVPProvidedByAbsent)
      const validationResult = await shaclService.validate(await shaclService.loadShaclFromUrl(CONTEXT_URL), serviceOffering)

      expect(validationResult).toEqual(expectedErrorResult)
    })
    it('returns false ServiceOffering without correct shape', async () => {
      const serviceOffering = await shaclService.loadFromJSONLDWithQuads(ServiceOfferingVPStructureInvalid)
      const validationResultFaulty = await shaclService.validate(await shaclService.loadShaclFromUrl(CONTEXT_URL), serviceOffering)

      expect(validationResultFaulty).toEqual(expectedErrorResult)
    })
  })

  function expectDatasetKeysToExist(dataset: any) {
    const keys = Object.keys(expectedDatasetObject)
    for (const key of keys) {
      expect(dataset[key]).toBeDefined()
    }
  }
})
