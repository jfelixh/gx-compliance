import { Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Cron, CronExpression } from '@nestjs/schedule'

import { DIDDocument } from 'did-resolver'

import { DEFAULT_VC_LIFE_EXPECTANCY_IN_DAYS } from '../constants'
import { ComplianceCredentialDto, CredentialSubjectDto, VerifiableCredentialDto, VerifiablePresentationDto } from '../dto'
import { CompliantCredentialSubjectDto } from '../dto/compliant-credential-subject.dto'
import { ComplianceCredentialMapper } from '../mapper/compliance-credential.mapper'
import { getDidWeb } from '../utils'
import { ExpirationDateService } from './expiration-date.service'
import { TimeService } from './time.service'

@Injectable()
export class EvidenceService {
  readonly logger = new Logger(EvidenceService.name)
  readonly didCache = new Map<string, DIDDocument>()
  readonly certificateCache = new Map<string, string>()

  constructor(
    private readonly configService: ConfigService,
    private readonly expirationDateService: ExpirationDateService,
    private readonly timeService: TimeService
  ) {}

  @Cron(CronExpression.EVERY_5_MINUTES)
  handleCron() {
    this.logger.log('Clearing caches')
    this.didCache.clear()
    this.certificateCache.clear()
  }

  /**
   * Creates a compliance credential and makes it a verifiable credential by signing it with
   * the Gaia-X signature algorithm (derived from <a href="https://www.w3.org/community/reports/credentials/CG-FINAL-lds-jws2020-20220721/">JsonWebSignature2020</a>).
   *
   * @param selfDescription the verifiable presentation containing the compliant credentials
   * @param vcid the optional ID to assign to the output verifiable credential instead of a generated ID
   * @returns a single verifiable credential with multiple compliance credentials in the
   * {@code credentialSubject} attribute.
   */
  async createComplianceCredential(
    selfDescription: VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>>,
    vcid: string
  ): Promise<VerifiableCredentialDto<ComplianceCredentialDto>> {
    const compliantCredentialSubjects = selfDescription.verifiableCredential.map(vc => new ComplianceCredentialMapper().map(vc))
    const expirationDate = await this.expirationDateService.getExpirationDateBasedOnCertOrThreshold(
      this.configService.get<string>('X509_CERTIFICATE'),
      Number.parseInt(process.env.vcLifeExpectancyInDays || DEFAULT_VC_LIFE_EXPECTANCY_IN_DAYS)
    )
    const issuanceDate: Date = await this.timeService.getNtpTime()
    const complianceCredential: VerifiableCredentialDto<CompliantCredentialSubjectDto> = {
      '@context': ['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'],
      type: ['VerifiableCredential', 'gx:ComplianceCredential'],
      id: vcid,
      issuer: getDidWeb(),
      validFrom: issuanceDate.toISOString(),
      validUntil: expirationDate.toISOString(),
      credentialSubject: {
        id: `${vcid}#cs`,
        'gx:evidence': compliantCredentialSubjects
      }
    }
    return complianceCredential as VerifiableCredentialDto<ComplianceCredentialDto>
  }
}
