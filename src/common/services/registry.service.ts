import { HttpService } from '@nestjs/axios'
import { Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { firstValueFrom } from 'rxjs'

@Injectable()
export class RegistryService {
  private readonly registryUrl: string
  private readonly logger = new Logger(RegistryService.name)

  constructor(private readonly httpService: HttpService, private readonly configService: ConfigService) {
    this.registryUrl = this.configService.get<string>('REGISTRY_URL', 'https://registry.gaia-x.eu/v2')
  }

  async isValidCertificateChain(raw: string): Promise<boolean> {
    try {
      // skip signature check against registry - NEVER ENABLE IN PRODUCTION
      if (this.configService.get('DISABLE_SIGNATURE_CHECK') === 'true') return true

      const response = await firstValueFrom(
        this.httpService.post(`${this.registryUrl}/api/trustAnchor/chain`, {
          certs: raw
        })
      )

      return response.status === 200
    } catch (error) {
      console.error(error)
      this.logger.error(error)
    }
  }

  async loadTurtleShapesFromUrl(shapesURL: string): Promise<string> {
    return (await firstValueFrom(this.httpService.get(shapesURL, { headers: { Accept: 'text/turtle' } }))).data
  }
}
