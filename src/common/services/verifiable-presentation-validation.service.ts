import { Injectable, Logger } from '@nestjs/common'

import { DateTime } from 'luxon'
import { v4 as uuidv4 } from 'uuid'

import { ValidationResult, VerifiableCredentialDto, VerifiablePresentationDto } from '../dto'
import { mergeResults } from '../utils/merge-results'
import { ShaclService } from './shacl.service'
import { TrustFramework2210ValidationService } from './tf2210/trust-framework-2210-validation.service'

export type VerifiablePresentation = VerifiablePresentationDto<VerifiableCredentialDto<any>>

@Injectable()
export class VerifiablePresentationValidationService {
  readonly logger = new Logger(VerifiablePresentationValidationService.name)

  constructor(private shaclService: ShaclService, private trustFramework2210ValidationService: TrustFramework2210ValidationService) {}

  static getUUIDStartingWithALetter() {
    let uuid = uuidv4()
    while (!isNaN(uuid[0])) {
      uuid = uuidv4()
    }
    return uuid
  }

  public async validateVerifiablePresentation(vp: VerifiablePresentation): Promise<ValidationResult> {
    const VPUUID = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    this.logger.debug('Processing VP ' + VPUUID, vp)
    const validationResult = this.validateVCsExpiration(vp)
    if (!validationResult.conforms) {
      this.logger.warn(`Dates validation failed ${VPUUID} ${JSON.stringify(validationResult.results)}`)
      return validationResult
    }
    // isValidCertificateChain
    this.logger.log(`Validate VP & VCs structure ${VPUUID}`)
    const structureResult = await this.validateVPAndVCsStructure(vp)
    if (!structureResult.conforms) {
      this.logger.warn(`Structural validation failed ${VPUUID} ${JSON.stringify(validationResult.results)}`)
      return mergeResults(validationResult, structureResult)
    }
    this.logger.log(`Validate business rules ${VPUUID}`)
    const businessRulesValidationResult = await this.validateBusinessRules(vp, VPUUID)
    if (!businessRulesValidationResult.conforms) {
      this.logger.warn(`Business validation failed ${VPUUID} ${JSON.stringify(businessRulesValidationResult.results)}`)
      return businessRulesValidationResult
    }
    this.logger.log(`Validation success ${VPUUID}`)
    return mergeResults(validationResult, structureResult, businessRulesValidationResult)
  }

  public async validateVPAndVCsStructure(vp: VerifiablePresentation): Promise<ValidationResult> {
    return await this.shaclService.verifyShape(vp)
  }

  public async validateBusinessRules(vp: VerifiablePresentation, VPUUID: string): Promise<ValidationResult> {
    return await this.trustFramework2210ValidationService.validate(vp, VPUUID)
  }

  private validateVCsExpiration(vp: VerifiablePresentation): ValidationResult {
    const validationResult: ValidationResult = new ValidationResult()
    vp.verifiableCredential.forEach(vc => {
      if (DateTime.fromISO(vc.validFrom) > DateTime.now()) {
        validationResult.conforms = false
        validationResult.results.push(`VC ${vc.id} validFrom ${vc.validFrom} is in the future`)
      }
      if (vc.validUntil) {
        if (DateTime.fromISO(vc.validUntil) < DateTime.now()) {
          validationResult.conforms = false
          validationResult.results.push(`VC ${vc.id} validUntil ${vc.validUntil} is in the past`)
        }
      }
    })
    return validationResult
  }
}
