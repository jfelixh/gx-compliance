import { HttpModule } from '@nestjs/axios'
import { Test } from '@nestjs/testing'

import { DateTime } from 'luxon'

import { CommonModule } from '../common.module'
import { ShaclService } from './shacl.service'
import { TrustFramework2210ValidationService } from './tf2210/trust-framework-2210-validation.service'
import { VerifiablePresentationValidationService } from './verifiable-presentation-validation.service'

describe('VerifiablePresentationValidationService', () => {
  let service: VerifiablePresentationValidationService
  beforeAll(async () => {
    const fixture = await Test.createTestingModule({
      imports: [CommonModule, HttpModule],
      providers: [VerifiablePresentationValidationService, ShaclService, TrustFramework2210ValidationService]
    })
      .overrideProvider(ShaclService)
      .useValue(jest.fn())
      .overrideProvider(TrustFramework2210ValidationService)
      .useValue(jest.fn())
      .compile()
    service = fixture.get(VerifiablePresentationValidationService)
  })

  it('should reject VC where validFrom is in the future', async () => {
    const results = await service.validateVerifiablePresentation({
      '@context': {},
      '@type': ['VerifiablePresentation'],
      verifiableCredential: [
        {
          '@context': {},
          type: ['VerifiableCredential'],
          issuer: null,
          validFrom: DateTime.now().plus({ days: 1 }).toISO(),
          validUntil: DateTime.now().plus({ days: 2 }).toISO(),
          credentialSubject: {}
        }
      ]
    })
    expect(results).toBeDefined()
    expect(results.conforms).toBeFalsy()
    expect(results.results).toBeDefined()
    expect(results.results).toHaveLength(1)
    expect(results.results[0]).toMatch(/VC .* validFrom .* is in the future/)
  })
  it('should reject VC where validUntil is in the past', async () => {
    const results = await service.validateVerifiablePresentation({
      '@context': {},
      '@type': ['VerifiablePresentation'],
      verifiableCredential: [
        {
          '@context': {},
          type: ['VerifiableCredential'],
          issuer: null,
          validFrom: DateTime.now().minus({ days: 2 }).toISO(),
          validUntil: DateTime.now().minus({ days: 1 }).toISO(),
          credentialSubject: {}
        }
      ]
    })
    expect(results).toBeDefined()
    expect(results.conforms).toBeFalsy()
    expect(results.results).toBeDefined()
    expect(results.results).toHaveLength(1)
    expect(results.results[0]).toMatch(/VC .* validUntil .* is in the past/)
  })
})
