export const JWT_VC_CONTENT_TYPES = ['application/vc+jwt', 'application/jwt']
export const JWT_VP_CONTENT_TYPE = 'application/vp+ld+jwt'
export const DEFAULT_VC_LIFE_EXPECTANCY_IN_DAYS = '90'
export const RULES_VERSION = 'PRLD-24.04_pre'
export const CTY_VALUE = 'vc+ld+json'
export const TYP_VALUE = 'vc+ld+json+jwt'
export const NORMALIZATION_ALG = 'RFC8785:JCS'
