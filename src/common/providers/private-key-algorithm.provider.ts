import { FactoryProvider } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

export class PrivateKeyAlgorithmProvider {
  static create(): FactoryProvider<string> {
    return {
      provide: 'privateKeyAlgorithm',
      inject: [ConfigService],
      useFactory: async (configService: ConfigService): Promise<string> => {
        return configService.get<string>('PRIVATE_KEY_ALG', 'PS256')
      }
    }
  }
}
