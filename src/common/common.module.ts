import { HttpModule } from '@nestjs/axios'
import { Global, Module, OnApplicationBootstrap } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { KeyLike } from 'jose'

import { CertificateExpirationBatch } from '../batch/certificate-expiration.batch'
import { ParticipantContentValidationService } from '../participant/services/participant-content-validation.service'
import { ServiceOfferingContentValidationService } from '../service-offering/services/service-offering-content-validation.service'
import { ServiceOfferingLabelLevelValidationService } from '../service-offering/services/service-offering-label-level-validation.service'
import { DidResolverProvider } from './providers/did-resolver.provider'
import { DocumentLoaderProvider } from './providers/document-loader.provider'
import { GaiaXSignatureSignerProvider } from './providers/gaia-x-signature-signer.provider'
import { GaiaXSignatureVerifierProvider } from './providers/gaia-x-signature-verifier.provider'
import { JsonWebSignature2020VerifierProvider } from './providers/json-web-signature-2020-verifier.provider'
import { PrivateKeyAlgorithmProvider } from './providers/private-key-algorithm.provider'
import { PrivateKeyProvider } from './providers/private-key.provider'
import { DIDService } from './services/did.service'
import { EvidenceService } from './services/evidence.service'
import { ExpirationDateService } from './services/expiration-date.service'
import { RegistryService } from './services/registry.service'
import { ShaclService } from './services/shacl.service'
import { TrustFramework2210ValidationService } from './services/tf2210/trust-framework-2210-validation.service'
import { TimeService } from './services/time.service'
import { VcQueryService } from './services/vc-query.service'
import { VerifiablePresentationValidationService } from './services/verifiable-presentation-validation.service'
import { CertificateUtil, createDidDocument } from './utils'

@Global()
@Module({
  imports: [HttpModule],
  providers: [
    ConfigService,
    CertificateExpirationBatch,
    DidResolverProvider.create(),
    DIDService,
    DocumentLoaderProvider.create(),
    ExpirationDateService,
    GaiaXSignatureSignerProvider.create(),
    GaiaXSignatureVerifierProvider.create(),
    JsonWebSignature2020VerifierProvider.create(),
    ParticipantContentValidationService,
    EvidenceService,
    RegistryService,
    ServiceOfferingContentValidationService,
    ServiceOfferingLabelLevelValidationService,
    PrivateKeyAlgorithmProvider.create(),
    PrivateKeyProvider.create(),
    ShaclService,
    TimeService,
    TrustFramework2210ValidationService,
    VcQueryService,
    VerifiablePresentationValidationService
  ],
  exports: [DIDService, EvidenceService, ShaclService, 'privateKey', 'privateKeyAlgorithm', VerifiablePresentationValidationService]
})
export class CommonModule implements OnApplicationBootstrap {
  constructor(private readonly configService: ConfigService) {}

  async onApplicationBootstrap() {
    const x509Certificate: string = this.configService.get<string>('X509_CERTIFICATE')
    const privateKeyAlg: string = this.configService.get<string>('PRIVATE_KEY_ALG', 'PS256')

    const certificate: KeyLike = await CertificateUtil.loadCertificate(x509Certificate)
    await createDidDocument(certificate, privateKeyAlg)
  }
}
