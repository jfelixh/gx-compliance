import { DateTime } from 'luxon'
import { pki } from 'node-forge'

export class CertificateBuilderUtilsSpec {
  static createCertificateAndKeyPair(validityThresholdInMonths: number): {
    cert: pki.Certificate
    keypair: pki.KeyPair
  } {
    const cert = pki.createCertificate()
    const keyPair = this.buildKeyPair()

    cert.publicKey = keyPair.publicKey
    cert.serialNumber = '01'
    cert.validity.notBefore = DateTime.now().toJSDate()
    cert.validity.notAfter = DateTime.now().plus({ months: validityThresholdInMonths }).toJSDate()

    const attrs: pki.CertificateField[] = [
      {
        name: 'commonName',
        value: 'dev.gaia-x.eu'
      },
      {
        name: 'countryName',
        value: 'BE'
      }
    ]
    cert.setSubject(attrs)
    cert.setIssuer(attrs)

    cert.sign(keyPair.privateKey)

    return { cert, keypair: keyPair }
  }

  static createCertificate(validityThresholdInMonths: number): pki.Certificate {
    return this.createCertificateAndKeyPair(validityThresholdInMonths).cert
  }

  static buildKeyPair(): pki.rsa.KeyPair {
    return pki.rsa.generateKeyPair(2048)
  }

  static convertToPKCS8(privateKey: pki.PrivateKey): string {
    const asn1PrivateKey = pki.privateKeyToAsn1(privateKey)
    const privateKeyInfo = pki.wrapRsaPrivateKey(asn1PrivateKey)

    return pki.privateKeyInfoToPem(privateKeyInfo)
  }
}

describe('CertificateBuilderForTests', () => {
  it('is used only to generate certs to test enddate for VC issuance', () => {
    // Nothing
  })
})
