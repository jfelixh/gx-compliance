export class ConfigServiceMock {
  private readonly properties: Record<string, string>

  constructor(properties: Record<string, string>) {
    this.properties = properties
  }

  get(property: string, defaultValue?: string): string {
    return this.properties[property] ?? defaultValue
  }
}
