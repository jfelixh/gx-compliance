import { Inject, Injectable } from '@nestjs/common'

import { KeyLike, SignJWT } from 'jose'

import { CTY_VALUE, TYP_VALUE } from '../../common/constants'
import { X509_VERIFICATION_METHOD_NAME, getDidWeb } from '../../common/utils'

@Injectable()
export class JwtSignatureService {
  constructor(@Inject('privateKey') private privateKey: KeyLike, @Inject('privateKeyAlgorithm') private privateKeyAlgorithm: string) {}

  async signVerifiableCredential(verifiableCredential: any): Promise<string> {
    const issuer = getDidWeb()

    const instance = new SignJWT(verifiableCredential).setProtectedHeader({
      alg: this.privateKeyAlgorithm,
      iss: issuer,
      kid: `${issuer}#${X509_VERIFICATION_METHOD_NAME}`,
      iat: new Date(verifiableCredential.validFrom).getTime(),
      exp: new Date(verifiableCredential.validUntil).getTime(),
      cty: CTY_VALUE,
      typ: TYP_VALUE
    })
    return instance.sign(this.privateKey)
  }
}
