import { Test } from '@nestjs/testing'

import { CommonModule } from '../../common/common.module'
import { JwtToVpService } from './jwt-to-vp.service'

describe('JWTToVPService', () => {
  let jwtToVpService: JwtToVpService
  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [CommonModule],
      providers: [JwtToVpService]
    }).compile()
    jwtToVpService = moduleFixture.get(JwtToVpService)
  })
  it('should return a VerifiablePresentation containing the VCs', () => {
    const results = jwtToVpService.JWTVcsToVP(vcs)
    expect(results).toBeDefined()
    expect(results.verifiableCredential).toBeDefined()
    expect(results.verifiableCredential).toHaveLength(vcs.length)
  })
})

const vcs = [
  {
    '@context': ['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'],
    id: 'https://example.com/participant.json',
    type: ['VerifiableCredential', 'gx:LegalParticipant'],
    issuer: 'did:web:bakeup.io',
    validFrom: '2024-01-01T19:23:24Z',
    credentialSubject: {
      id: 'https://example.com/participant.json#cs',
      type: 'gx:LegalParticipant',
      'gx:legalName': 'Example Ltd.',
      'gx:headquarterAddress': {
        'gx:countrySubdivisionCode': 'FR-HDF'
      },
      'gx:legalAddress': {
        'gx:countrySubdivisionCode': 'FR-HDF'
      },
      'gx:legalRegistrationNumber': {
        id: 'https://example.com/lrn.json#cs'
      }
    },
    default: {
      '@context': ['https://www.w3.org/ns/credentials/v2', 'https://w3id.org/gaia-x/development#'],
      id: 'https://example.com/participant.json',
      type: ['VerifiableCredential', 'gx:LegalParticipant'],
      issuer: 'did:web:bakeup.io',
      validFrom: '2024-01-01T19:23:24Z',
      credentialSubject: {
        id: 'https://example.com/participant.json#cs',
        type: 'gx:LegalParticipant',
        'gx:legalName': 'Example Ltd.',
        'gx:headquarterAddress': {
          'gx:countrySubdivisionCode': 'FR-HDF'
        },
        'gx:legalAddress': {
          'gx:countrySubdivisionCode': 'FR-HDF'
        },
        'gx:legalRegistrationNumber': {
          id: 'https://example.com/lrn.json#cs'
        }
      }
    }
  }
]
