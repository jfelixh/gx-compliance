import { BadRequestException } from '@nestjs/common'
import { Test } from '@nestjs/testing'

import { JWK, KeyLike, importJWK } from 'jose'

import { CommonModule } from '../../common/common.module'
import { DIDService } from '../../common/services/did.service'
import * as vc from '../../tests/fixtures/v2/vc.json'
import { VCJwtUtilsSpec } from '../../tests/vc-jwt-utils.spec'
import { JwtSignatureValidationService } from './jwt-signature-validation.service'

describe('JWTSignatureValidationService', () => {
  let jwtSignatureValidationService: JwtSignatureValidationService

  let jwtMissingIss: string
  let jwtMissingKid: string
  let jwtWithAllFields: string
  let jwk: JWK

  const didServiceMock = {
    getDIDDocumentFromDID: jest.fn(() => {
      return {
        verificationMethod: [
          {
            id: 'did:web:example.com#key',
            publicKeyJwk: jwk
          }
        ]
      }
    }),
    getJWKFromDID: jest.fn(() => {
      return jwk
    }),
    getPublicKeyFromJWK: jest.fn(async () => {
      return <KeyLike>await importJWK(jwk)
    })
  }
  beforeAll(async () => {
    const jwts = await VCJwtUtilsSpec.preparePayloadsAndKeys()
    jwtWithAllFields = jwts.jwtVP
    jwtMissingIss = jwts.jwtVCWithoutIss
    jwtMissingKid = jwts.jwtVCWithoutKid
    jwk = jwts.jwk
    const testingModule = await Test.createTestingModule({
      imports: [CommonModule],
      providers: [JwtSignatureValidationService]
    })
      .overrideProvider(DIDService)
      .useValue(didServiceMock)
      .compile()
    jwtSignatureValidationService = testingModule.get<JwtSignatureValidationService>(JwtSignatureValidationService)
  })

  it('should validate JWT signature using iss to retrieve key and kid to get the verificationMethod', async () => {
    const decodingResults = await jwtSignatureValidationService.validateJWTAndConvertToVCs(jwtWithAllFields)
    expect(decodingResults).toBeDefined()
    expect(decodingResults.verifiableCredentials).toHaveLength(1)
    expect(decodingResults.verifiableCredentials[0]).toEqual(vc)
  })

  it('should return BadRequestException when the JWT cannot be decoded', async () => {
    try {
      await jwtSignatureValidationService.validateJWTAndConvertToVCs('toto')
      fail('should have thrown an exception')
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException)
      expect((<BadRequestException>error).message).toBe('Invalid payload')
    }
  })
  it('should return BadRequestException when the JWT signature has been modified', async () => {
    try {
      await jwtSignatureValidationService.validateJWTAndConvertToVCs(jwtWithAllFields.substring(0, jwtWithAllFields.length - 1))
      fail('should have thrown an exception')
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException)
      expect((<BadRequestException>error).message).toBe('JWSSignatureVerificationFailed')
    }
  })
  it('should return BadRequestException when the iss header is missing in the JWT', async () => {
    try {
      await jwtSignatureValidationService.validateJWTAndConvertToVCs(jwtMissingIss)
      fail('should have thrown an exception')
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException)
      expect((<BadRequestException>error).message).toBe('Invalid request')
    }
  })
  it('should return BadRequestException when the kid header is missing in the JWT', async () => {
    try {
      await jwtSignatureValidationService.validateJWTAndConvertToVCs(jwtMissingKid)
      fail('should have thrown an exception')
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException)
      expect((<BadRequestException>error).message).toBe('Invalid request')
    }
  })
})
