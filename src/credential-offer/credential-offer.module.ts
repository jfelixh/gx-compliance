import { Module } from '@nestjs/common'

import { CommonModule } from '../common/common.module'
import { JwtModule } from '../jwt/jwt.module'
import { CredentialOfferController } from './controller/credential-offer.controller'
import { CredentialOfferService } from './service/credential-offer.service'

@Module({
  imports: [CommonModule, JwtModule],
  providers: [CredentialOfferService],
  controllers: [CredentialOfferController]
})
export class CredentialOfferModule {}
