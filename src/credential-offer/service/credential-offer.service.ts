import { ConflictException, Injectable, Logger } from '@nestjs/common'

import { DIDService } from '../../common/services/did.service'
import { EvidenceService } from '../../common/services/evidence.service'
import { VerifiablePresentationValidationService } from '../../common/services/verifiable-presentation-validation.service'
import { JwtSignatureValidationService } from '../../jwt/service/jwt-signature-validation.service'
import { JwtSignatureService } from '../../jwt/service/jwt-signature.service'
import { JwtToVpService } from '../../jwt/service/jwt-to-vp.service'

@Injectable()
export class CredentialOfferService {
  private logger = new Logger(CredentialOfferService.name)

  constructor(
    private readonly didService: DIDService,
    private readonly jwtSignatureService: JwtSignatureService,
    private readonly jwtSignatureValidationService: JwtSignatureValidationService,
    private readonly jwtToVPService: JwtToVpService,
    private readonly proofService: EvidenceService,
    private readonly verifiablePresentationValidationService: VerifiablePresentationValidationService
  ) {}

  public async checkAndIssueComplianceCredential(jwtVerifiablePresentation: string, outputVCID: string): Promise<string> {
    const { verifiableCredentials, key, JWK } = await this.jwtSignatureValidationService.validateJWTAndConvertToVCs(jwtVerifiablePresentation)
    await this.didService.checkx5uMatchesPublicKey(key, JWK.x5u)
    const verifiablePresentation = this.jwtToVPService.JWTVcsToVP(verifiableCredentials)
    const results = await this.verifiablePresentationValidationService.validateVerifiablePresentation(verifiablePresentation)
    if (results.conforms) {
      const complianceCredentialDTO = await this.proofService.createComplianceCredential(verifiablePresentation, outputVCID)
      return await this.jwtSignatureService.signVerifiableCredential(complianceCredentialDTO)
    } else {
      throw new ConflictException('Unable to validate compliance', results.results.join(','))
    }
  }
}
